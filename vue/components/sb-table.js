Vue.component("sb-table", {
    props: {
        cols1: {
            required: true
        },
        rows1: {
            required: true
        }
    },
    data() {
        return {
            cols: [],
            rows: []
        }
    },
    watch: {
        rows1() {
            this.rows = this.rows1.map(item => {
                return item
            })

            var orderCol = this.cols.filter((item) => {
                return item.sortType !== orderType.NORMAL
            })

            if (orderCol.length > 0) {
                var key = orderCol[0].key
                var type = orderCol[0].sortType

                this.rows.sort((a, b) => {
                    if (type === orderType.ASC) {
                        return a[key] > b[key] ? 1 : -1
                    } else {
                        return a[key] > b[key] ? -1 : 1
                    }
                })
            }

        }
    },
    methods: {
        order(type, key) {
            this.cols.forEach((item, index) => {
                item.sortType = orderType.NORMAL
                if (item.key === key) {
                    item.sortType = type
                    this.$set(this.cols, index, item)
                }
            });

            this.rows.sort((a, b) => {
                if (type === orderType.ASC) {
                    return a[key] > b[key] ? 1 : -1
                } else {
                    return a[key] > b[key] ? -1 : 1
                }
            })
        }
    },
    mounted() {
        this.cols = this.cols1.map(item => {
            item.sortType = orderType.NORMAL
            return item
        })
        this.rows = this.rows1.map(item => {
            return item
        })
    },
    render(h) {
        var that = this
        var createOrderArrow = function (type, text, key, isActive) {
            return h("strong", {
                'class': {
                    arrow: true,
                    active: isActive
                },
                on: {
                    click() {
                        that.order(type, key)
                    }
                }
            }, text)
        }

        var trs = []
        var ths = []
        this.cols.forEach(item => {

            var titles = []
            titles.push(h("span", item.title))
            if (item.sortable) {
                titles.push(createOrderArrow(orderType.ASC, "↑", item.key, item.sortType === orderType.ASC))
                titles.push(createOrderArrow(orderType.DESC, "↓", item.key, item.sortType === orderType.DESC))
            }
            var th = h("th", titles)
            ths.push(th)
        });
        //生成第一行，表头
        var tr1 = h("tr", ths)
        trs.push(tr1)

        //生成数据部分
        this.rows.forEach(item => {
            var tr = h("tr", [
                h("td", item.name),
                h("td", item.age),
                h("td", item.score)
            ])
            trs.push(tr)
        });

        return h("table", {
            'class': {
                "sb-table": true
            }
        }, trs)
    }
})