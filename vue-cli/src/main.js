import Vue from 'vue'
import App from './App.vue'
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import VueRouter from 'vue-router'
Vue.use(VueRouter)
Vue.use(iView)
import UserList from './components/page/user/list.vue'
import AddUser from './components/page/user/add.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: new VueRouter({
    routes: [{
      path: '/adduser',
      component: AddUser
    },{
      path: '/userlist',
      component: UserList
    }]
  })
}).$mount('#app')